package com.app.api.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "result_detail")
public class ResultDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1712065119232369093L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long profundidad;
	private Long number;
	private Long proceso;

	private Long next;

	public Long getNext() {
		return next;
	}

	public void setNext(Long next) {
		this.next = next;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProfundidad() {
		return profundidad;
	}

	public void setProfundidad(Long profundidad) {
		this.profundidad = profundidad;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public Long getProceso() {
		return proceso;
	}

	public void setProceso(Long proceso) {
		this.proceso = proceso;
	}

}
