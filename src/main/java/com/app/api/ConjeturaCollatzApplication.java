package com.app.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConjeturaCollatzApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConjeturaCollatzApplication.class, args);
	}

}
