package com.app.api.dao;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.api.entity.ResultDetail;
public interface ResultDetailDao extends CrudRepository<ResultDetail, Long>{

	@Query( value="select * from result_detail u where u.number=?1 order by u.id asc" ,nativeQuery = true)
	public ResultDetail getNumber(Long number);
	
	@Query( value="select * from result_detail u where u.next=?1 order by u.id asc" ,nativeQuery = true)
	public Collection<ResultDetail> getNext(Long next);
	
	
	@Query( value="select * from result_detail u where u.proceso=?1 order by u.number desc" ,nativeQuery = true)
	public Collection<ResultDetail> listByPadre(Long idPadre);
	
	@Query( value="select * from result_detail u where u.proceso=?1 and u.id>?2 order by u.id asc" ,nativeQuery = true)
	public Collection<ResultDetail> listByIDProceso(Long padre, Long number);
	
	@Query( value="select * from result_detail u where u.id>?1 order by u.id desc" ,nativeQuery = true)
	public Collection<ResultDetail> listByIDOrd( Long number);
	
	@Query( value="select * from result_detail u where u.number=?1 order by u.id desc" ,nativeQuery = true)
	public Collection<ResultDetail> listByNumber( Long number);
	
	public ResultDetail findByNumber(Long number);
	
	
}
