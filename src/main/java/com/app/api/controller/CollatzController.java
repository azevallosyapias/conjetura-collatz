package com.app.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.api.service.ICollatz;

import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("api/v1/collatz")
public class CollatzController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ICollatz coll;

	@ApiOperation(value = "Generando La Conjetura de Collatz por Parametro")
	@GetMapping(value = "/{id}")
	public String getCollatz(@PathVariable Long id) {
		logger.info("numero inicial: " + id);
		return coll.getC(id);
	}

	@ApiOperation(value = "Generando La Conjetura de Collatz por valor Aleatorio")
	@GetMapping(value = "/random")
	public String getCollatz() {
		logger.info("Collatz Random: ");
		return coll.getC();
	}

	@ApiOperation(value = "Obteniendo la profundidad de los valores procesados")
	@GetMapping(value = "/consulta/{inicial}/{profundidad}")
	public String getProfundidad(@PathVariable Long inicial, @PathVariable Long profundidad) {
		logger.info("Obteniendo Profundidad: " + inicial + " - " + profundidad);
		List<Long> procesados = new ArrayList<Long>();
		return coll.getProf(inicial, profundidad, procesados);
	}

}
