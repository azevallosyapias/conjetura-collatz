package com.app.api.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.api.dao.ResultDetailDao;
import com.app.api.entity.ResultDetail;
import com.google.gson.Gson;

@Service
public class ICollatzImpl implements ICollatz {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ResultDetailDao detailDao;

	@Override
	public String getC() {

		Integer initial = new Double(Math.random() * 1000).intValue();

		if (initial == 0) {
			initial = 1;
		}

		logger.info("NUMERO: " + initial);
		return getC(new Long(initial));
	}

	@Override
	public String getC(Long inicial) {

		long iniO = inicial;
		long prof2 = 0l;
		boolean procesar = true;
		long p = 1;
		List<ResultDetail> procesados = new ArrayList<ResultDetail>();
		while (procesar) {
			Long nextV;
			if (inicial % 2 == 0) {
				nextV = inicial / 2;
			} else {
				nextV = inicial * 3 + 1;
			}
			if (inicial == 1) {
				procesar = false;
				prof2 = procesados.size();
			}
			// buscando el numero
			ResultDetail bean = detailDao.getNumber(inicial);
			if (bean != null) {
				// encontro
				procesar = false;
			} else {
				bean = new ResultDetail();
				bean.setNumber(inicial);
				bean.setNext(nextV);
				ResultDetail beanNext = detailDao.getNumber(nextV);
				if (beanNext != null) {
					p = beanNext.getProfundidad() + 1;
					prof2 = beanNext.getProfundidad() + 1 + procesados.size();
					procesar = false;
				}
				bean.setProfundidad(new Long(p));
				procesados.add(bean);

			}
			p++;
			inicial = nextV;
		}

		for (ResultDetail bean : procesados) {
			bean.setProfundidad(prof2);
			if (bean.getProfundidad().equals(0L)) {
				bean.setNext(null);
			}
			detailDao.save(bean);
			logger.info("registro: " + new Gson().toJson(bean));
			prof2--;
		}

		boolean leer = true;
		String respuesta = "";
		// respuesta = respuesta + " -> "+inicial;
		logger.info("inicial: " + iniO);
		while (leer) {
			ResultDetail detail = detailDao.findByNumber(iniO);
			logger.info("ver: " + new Gson().toJson(detail));
			respuesta = respuesta + " -> " + detail.getNumber();
			if (detail.getNext() == null) {
				leer = false;
			} else {
				iniO = detail.getNext();
			}

		}
		return respuesta;
	}

	@Override
	public String getProf(Long inicial, Long profundidad, List<Long> procesados) {
		String respuesta = "";
		boolean procesar = true;
		if (!procesados.contains(inicial)) {
			procesados.add(inicial);
		} else {
			procesar = false;
		}

		if (procesar) {
			ResultDetail bean = detailDao.getNumber(new Long(inicial));
			if (bean != null) {
				logger.info("Datos: " + new Gson().toJson(bean) + " profundidad: " + profundidad);
				respuesta = respuesta + " - " + bean.getNumber() + "\n";
				for (int i = 0; i < profundidad; i++) {
					List<ResultDetail> beanNext = (List<ResultDetail>) detailDao.getNext(bean.getNumber());
					for (ResultDetail beanN : beanNext) {
						if (profundidad > 1) {
							respuesta = respuesta + getProf(beanN.getNumber(), profundidad - 1, procesados);
						}
					}
				}
			} else {
				logger.info("No se encuentra registrado el resultado en el sistema");
			}
		}

		return respuesta;
	}

}
