package com.app.api.service;

import java.util.List;

public interface ICollatz {

	public String getC();
	public String getC(Long numero);
	public String getProf(Long inicial, Long profundidad, List<Long> lista);
	
}
